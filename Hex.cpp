/*
	Hex Program

	Program should use a graph representation and treat the game as a path finding problem. Each internal node (hexagon) has six neighbors � so each would have 6 edges. The corners and the edges are special. A corner has either two or three neighbors and a non-corner edge has 4 neighbors.

	The player should be able to interact with the program and choose its �color� with blue going first. The program should have a convenient interface for entering a move, displaying the board, and then making its own move. The program should determine when the game is over and announce the winner.

	A simple board display would be to have an 11 x 11 printout with B, R, or a blank in each position. A simple way to input a move would be to have the player enter an (i,j) coordinate corresponding to a currently empty hexagon and have the program check that this is legal and if not ask for another choice.

	HW 4 expectations:
	Be able to draw the board using ASCII symbols and a given size, such as 7 by 7 or 11 by 11.
	Input a move and determine if a move is legal.
	Determine who won.

	Made by Oderick van Dongen
*/

#include<iostream>
#include<vector>
#include<string>


// ---------------------------------- Classes ---------------------------------- //
enum class Occupation : short { Empty, Red, Blue };

class Hex {
private:
	int row;
	int column;
	Occupation occupation;

	// << override
	friend std::ostream& operator<<(std::ostream& output, Hex& const hex) {
		char occupiedBy;
		switch (hex.get_occupation())
		{
		case Occupation::Empty:
			occupiedBy = '.';
			break;
		case Occupation::Red:
			occupiedBy = 'R';
			break;
		case Occupation::Blue:
			occupiedBy = 'B';
			break;
		default:
			break;
		}

		output << "Hex(" << hex.get_row() << ", " << hex.get_column() << ")" << occupiedBy;
		return output;
	}

public:
	// Constructor & Destructor
	Hex(int row, int column) :row(row), column(column) { occupation = Occupation::Empty; }
	~Hex() {}

	// Functions
	int get_row() { return row; }
	int get_column() { return column; }
	Occupation get_occupation() { return occupation; }
	char get_char_representation() {
		switch (occupation)
		{
		case Occupation::Empty:
			return '.';
		case Occupation::Red:
			return 'R';
		case Occupation::Blue:
			return 'B';
		default:
			break;
		}
	}

	void set_row(int row) { this->row = row; }
	void set_column(int column) { this->column = column; }
	void set_occupation(Occupation newOccupation) { occupation = newOccupation; }
};

class Connection {
private:
	Hex* hex1;
	Hex* hex2;

	// << override
	friend std::ostream& operator<<(std::ostream& output, Connection& const connection) {

		output << "Edge(" << *connection.get_hex1() << ", " << *connection.get_hex2() << ")";
		return output;
	}

public:
	// Constructor & Destructor
	Connection(Hex* hex1, Hex* hex2) :hex1(hex1), hex2(hex2) {}
	~Connection() {}

	// Functions
	Hex* get_hex1() { return hex1; }
	Hex* get_hex2() { return hex2; }
};

class GameBoard {
private:
	std::vector<Hex*> hexes;
	std::vector<Connection*> connections;

	friend std::ostream& operator<<(std::ostream& output, GameBoard const gameboard) {
		int totalHexes = gameboard.hexes.size();
		int dimension = sqrt(totalHexes);

		// --- Ascii representation --- //
		output << "Print out ascii board:\n\nRows\n";
		std::string spaces = "\t";

		for (int i = 0; i < totalHexes; i++)
		{
			// Indentation
			if ((i + 1) % dimension == 1)
			{
				output << " " << ((i + 1) / dimension) + 1 << spaces;		// indent with numbering for clarity
				spaces += " ";
			}

			// Actual occupancy
			output << gameboard.hexes.at(i)->get_char_representation();

			if ((i + 1) % dimension > 0)
			{
				// Represent horizontal connections between hexes
				output << " - ";
			}
			else if ((i + 1) % (totalHexes) == 0)
			{
				break;					// Last row, no edges underneath required
			}
			else
			{
				output << "\n";

				output << spaces;		// indent
				spaces += " ";

				// Represent vertical connections inbetween rows of hexes
				for (int j = 0; j < dimension; j++)
				{
					output << "\\ ";
					if ((j + 1) % dimension > 0)
					{
						output << "/ ";
					}
				}
				output << "\n";
			}
		}
		// --- End of Ascii representation --- //

		return output;
	}

public:
	// Constructor & Destructor
	GameBoard(int dimension)
	{
		// Create hexes
		for (int i = 0; i < dimension; i++)
		{
			for (int j = 0; j < dimension; j++)
			{
				add_hex(i + 1, j + 1);
			}
		}

		// Create connections for hexes
		for (int i = 1; i <= dimension; i++)
		{
			for (int j = 1; j <= dimension; j++)
			{
				// Add connections to the hexes depending where the hex is on the board
				if (i == 1 && j == 1)						// First hex
				{
					add_connection(get_hexes().at(0), get_hexes().at(1));
					add_connection(get_hexes().at(0), get_hexes().at(dimension));
				}
				else if (i == 1 && j == dimension)			// Last hex first row
				{
					add_connection(get_hexes().at(dimension - 1),
						get_hexes().at((dimension * 2) - 2));
					add_connection(get_hexes().at(dimension - 1),
						get_hexes().at((dimension * 2) - 1));
				}
				else if (i == dimension && j == 1)			// First hex last row
				{
					add_connection(get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i - 1) * dimension + j));
				}
				else if (i == dimension && j == dimension)	// Last hex last row
				{
					// We add this if statement to specifically do nothing in this case
					// Required connections will be made from the surrounding hexes
				}
				else if (j == 1)							// Hex on the left edge of the board
				{
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i - 1) * dimension + j));
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i * dimension) + (j - 1)));
				}
				else if (i == dimension)					// Hex on bottom line
				{
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i - 1) * dimension + j));
				}
				else if (j == dimension)					// Hex on the right edge of the board
				{
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i * dimension) + (j - 2)));
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i * dimension) + (j - 1)));
				}
				else										// Middle hexes
				{
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i - 1) * dimension + j));
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i * dimension) + (j - 2)));
					add_connection(
						get_hexes().at((i - 1) * dimension + (j - 1)),
						get_hexes().at((i * dimension) + (j - 1)));
				}
			}
		}
	}
	~GameBoard() {}

	// Functions
	std::vector<Hex*> get_hexes() { return hexes; }
	std::vector<Connection*> get_connections() { return connections; }

	void add_hex(int row, int column) { hexes.push_back(new Hex{ row,column }); }
	void add_connection(Hex* hex1, Hex* hex2) { connections.push_back(new Connection(hex1, hex2)); }

	bool occupy_hex(Hex* hex, Occupation occupation) {
		// If hex is not occupied yet, occupy it
		if (hex->get_occupation() == Occupation::Empty)
		{
			hex->set_occupation(occupation);
			return true;
		}
		else
		{
			std::cout << *hex << " is already occupied!\n";
			return false;
		}
	}
	void choose_hex(Occupation occupation, std::string playerName, int dimension) {
		bool alreadyOccupied{ false };
		do
		{
			// Select a row to occupy a hex in
			std::cout << playerName << " - What row do you want to occupy a hex in: ";
			int row;
			do
			{
				std::cin >> std::noskipws >> row;
				std::cin.ignore();
				if (row <= 0 || row > dimension)
				{
					std::cout << "\nPlease give a valid row: ";
					std::cin >> std::noskipws >> row;
					std::cin.ignore();
				}
			} while (row <= 0 || row > dimension);
			// Select a column to occupy a hex in
			std::cout << playerName << " - What column do you want to occupy a hex in: ";
			int column;
			do
			{
				std::cin >> std::noskipws >> column;
				std::cin.ignore();
				if (column <= 0 || column > dimension)
				{
					std::cout << "\nPlease give a valid column: ";
					std::cin >> std::noskipws >> column;
					std::cin.ignore();
				}
			} while (column <= 0 || column > dimension);
			std::cout << "\n";

			// Calculation for selecting the correct hex without required to loop through all of them
			Hex* hex = hexes.at((row - 1) * dimension + (column - 1));
			alreadyOccupied = !occupy_hex(hex, occupation);

		} while (alreadyOccupied);
		std::cout << "\n\n";
	}

	std::vector<Hex*> get_connected_hexes(std::vector<Hex*> previousConnected, Hex* hex) {

		for (Connection* connection : connections)
		{
			// Check  through all connections with the parameter hex as "Hex1"
			if (connection->get_hex1() == hex &&
				connection->get_hex2()->get_occupation() == hex->get_occupation())
			{
				bool connected{ false };

				// Check if Hex 2 is already connected to
				for (Hex* passedHex : previousConnected)
				{
					if (connection->get_hex2() == passedHex) { connected = true; break; }
				}
				// If not connected already, add it now to the connected hexes
				if (!connected)
				{
					previousConnected.push_back(connection->get_hex2());
				}
			}
			// Check  through all connections with the parameter hex as "Hex2"
			else if (connection->get_hex2() == hex &&
				connection->get_hex1()->get_occupation() == hex->get_occupation())
			{
				bool connected{ false };
				// Check if Hex 1 is already connected to
				for (Hex* passedHex : previousConnected)
				{
					if (connection->get_hex1() == passedHex) { connected = true; break; }

				}
				// If not connected already, add it now to the connected hexes
				if (!connected)
				{
					previousConnected.push_back(connection->get_hex1());
				}
			}
		}
		return previousConnected;
	}
	Occupation check_win_status(int dimension) {
		// Per hex
		for (Hex* hex : hexes)
		{
			// Check if on left edge and board occupied by blue
			if (hex->get_column() == 1 && hex->get_occupation() == Occupation::Blue
				|| // OR if top edge of board and occupied by red
				hex->get_row() == 1 && hex->get_occupation() == Occupation::Red)
			{
				// look for a adjacent hexes until no more or opposite edge from start (Blue == right, Red == Bottom)
				std::vector<Hex*> passedHexes{};
				passedHexes.push_back(hex);

				std::vector<Hex*> newPassedHexes = passedHexes;

				bool done{ false };

				do
				{
					passedHexes.assign(newPassedHexes.begin(), newPassedHexes.end());
					for (int i = 0; i < passedHexes.size(); i++)
					{
						newPassedHexes = get_connected_hexes(newPassedHexes, newPassedHexes.at(i));
						// If hex found on opposite side in a path, return winning player
						if ((newPassedHexes.at(i)->get_column() == dimension &&
							newPassedHexes.at(i)->get_occupation() == Occupation::Blue)
							||
							(newPassedHexes.at(i)->get_row() == dimension &&
								newPassedHexes.at(i)->get_occupation() == Occupation::Red))
						{
							return newPassedHexes.at(i)->get_occupation();
						}
					}
				} while (newPassedHexes != passedHexes);
			}
		}
		// If no winning hexes were found, return empty to signify nobody won yet
		return Occupation::Empty;
	}

	Hex* calculate_best_move(Occupation occupation, int dimension) {
		std::vector<int> winningHexes(hexes.size());
		int calculations{ 0 };

		// repeat the following (minimum 1000)5000 times to calculate a good result
		do
		{
			// Make a new gameboard to test in
			GameBoard* testGameboard = new GameBoard(dimension);
			for (int i = 0; i < hexes.size(); i++)
			{
				if (hexes.at(i)->get_occupation() != Occupation::Empty)
				{
					testGameboard->hexes.at(i)->set_occupation(hexes.at(i)->get_occupation());
				}
			}

			int turnCounter{ 0 };
			bool hexesEmpty{ false };
			Occupation opponentOccupation;

			// Set opponent occupation for calculation purposes when filling in the test gameBoard
			if (occupation == Occupation::Blue)
			{
				opponentOccupation = Occupation::Red;
			}
			else
			{
				opponentOccupation = Occupation::Blue;
			}

			// Fill up all empty hexes at random on the board until someone wins
			do
			{
				hexesEmpty = false;

				// Occupy a random hex for each player by turn
				bool hexOccupied{ false };

				do
				{
					int randomNumber = rand() % (dimension * dimension);
					if (testGameboard->hexes.at(randomNumber)->get_occupation() == Occupation::Empty)
					{
						if (turnCounter % 2 == 0)
						{
							testGameboard->hexes.at(randomNumber)->set_occupation(occupation);
						}
						else
						{
							testGameboard->hexes.at(randomNumber)->set_occupation(opponentOccupation);
						}
						turnCounter++;
					}
					hexOccupied = true;
				} while (!hexOccupied);

				// Check if all hexes are filled in
				for (Hex* hex : testGameboard->hexes)
				{
					if (hex->get_occupation() == Occupation::Empty)
					{
						hexesEmpty = true;
					}
				}
			} while (hexesEmpty);

			bool gameWon = testGameboard->check_win_status(dimension) == occupation ? true : false;

			// Add the winning hexes count to keep up how many times each hex won
			if (gameWon)
			{
				for (int i = 0; i < testGameboard->hexes.size(); i++)
				{
					if (testGameboard->hexes.at(i) &&
						testGameboard->hexes.at(i)->get_occupation() == occupation)
					{
						winningHexes.at(i)++;
					}
				}
			}
			calculations++;

		} while (calculations < 5000);

		// Get hex with highest winrate
		Hex* bestHex{ hexes.at(0) };
		int bestWinrate{ 0 };

		for (int i = 0; i < winningHexes.size(); i++)
		{
			if (winningHexes.at(i) > bestWinrate &&
				hexes.at(i)->get_occupation() == Occupation::Empty)
			{
				bestWinrate = winningHexes.at(i);
				bestHex = hexes.at(i);
			}
		}
		return bestHex;
	}
};

// ---------------------------------- Main ---------------------------------- //
int main() {
	srand(time(nullptr)); //Seed srand()

	int turn{ 1 };
	std::string playerWon{ "" };

	// --- Player choice: board size
	int dimension;
	std::cout << "Welcome to HEX : the game!!!\n\n"
		<< "Please choose a board dimension from 3 to 20 (ex: 3 = board of 3 by 3 || 9 hexes): ";
	std::cin >> std::noskipws >> dimension;
	std::cin.ignore();
	std::cout << "\n";

	if (dimension < 3 || dimension > 20)
	{
		std::cout << "This dimension is not valid. We default back to 3.\n";
		dimension = 3;
	}

	GameBoard* board = new GameBoard(dimension);

	// --- Player choice: Player versus AI/player versus other player
	bool versusAI{ false };
	std::string versus;

	std::cout << "Do you want to play against AI? (Y/N): ";
	std::getline(std::cin, versus);
	std::cout << "\n\n";

	if (versus == "y" || versus == "Y")
	{
		versusAI = true;
	}

	// --- Player choice: names
	std::string playerBlue;
	std::string playerRed;

	std::cout << "What is the name of the Blue player: ";
	std::getline(std::cin, playerBlue);
	playerBlue += "(B)";
	std::cout << "\n\n";

	if (!versusAI)
	{
		std::cout << "What is the name of the Red player: ";
		std::getline(std::cin, playerRed);
		playerRed += "(R)";
		std::cout << "\n\n";
	}
	else
	{
		playerRed = "AI(R)";
	}

	// --- Game
	bool gameOver{ false };
	Occupation hexWon{ Occupation::Empty };
	do
	{
		// --- Board print
		std::cout << "-------------\nRules:\n\n"
			<< "Each player get's to turn an empty hex to his or her color by turn\n"
			<< "Blue player tries to form a line from side to side\n"
			<< "Red player tries to form a line from top to bottom or bottom to top\n"
			<< "First player to finish his or her line, wins the game\n"
			<< "-------------\n\n"
			<< "Turn: " << turn << "\n\n"
			<< *board << "\n\n";

		// --- Player choice: occupy hex
		if (turn % 2 != 0)
		{
			board->choose_hex(Occupation::Blue, playerBlue, dimension);
		}
		else if (versusAI)
		{
			// AI turn
			std::cout << "AI is thinking...\n";
			board->occupy_hex(board->calculate_best_move(Occupation::Red, dimension), Occupation::Red);
		}
		else
		{
			board->choose_hex(Occupation::Red, playerRed, dimension);
		}
		turn++;

		// --- Win status check
		hexWon = board->check_win_status(dimension);
		if (hexWon != Occupation::Empty)
		{
			gameOver = true;
			if (hexWon == Occupation::Blue)
			{
				playerWon = playerBlue;
			}
			else if (hexWon == Occupation::Red)
			{
				playerWon = playerRed;
			}
		}
	} while (!gameOver);

	// Post game
	std::cout << *board << "\n";

	std::cout << "GAME OVER!!!\n\n"
		<< "Player won: " << playerWon << "\n";
	if (!versusAI || (hexWon == Occupation::Blue && versusAI))
	{
		std::cout << "Good job!\n\n";
	}
	else
	{
		std::cout << "Better luck next time!";
	}

	return 0;
}